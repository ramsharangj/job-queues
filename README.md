# job-queues

A queue that assigns jobs to agents.

## Tech Assumptions
- Using an atom to represent all the domains.
- Pardon all the crud to the atom. It may not be the most efficient way.
- Could have used transducers for filtering. Not an expert at that concept. So I left it alone.
- Unit testing has been done for all the public functions. The unit tests also cover the behaviour oof the private functions.
- The general convention when a function is made public is to write a test and make it public.
- Could have used macros for some functions.
But did not want invest too much time for very less profits.

## Running the job queue

- `lein run file-path`. Ex `lein run /path/to/resources/sample-input.json`
- You can also run the tests. `core_test.clj` has the integration tests.
You can see the behaviour there.
