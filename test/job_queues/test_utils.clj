(ns job-queues.test-utils
  (:require [clojure.test :refer :all]
            [job-queues.domain.job :as job]
            [job-queues.domain.agent :as agent]))

(defn setup-tests [f]
  (reset! job/jobs [])
  (reset! agent/agents [])
  (f))
