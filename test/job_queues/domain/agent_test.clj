(ns job-queues.domain.agent-test
  (:require [clojure.test :refer :all]
            [job-queues.domain.agent :refer :all]
            [job-queues.test-utils :as utils]))

(use-fixtures :each utils/setup-tests)

(deftest create-agent-test
  (testing "Create Agents"
    (let [agents-data [{
                        :id "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88",
                        :name "Mr. Peanut Butter",
                        :primary_skillset ["rewards-question"],
                        :secondary_skillset ["bills-questions"]
                        }
                       {
                        :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                        :name "BoJack Horseman",
                        :primary_skillset ["bills-questions"],
                        :secondary_skillset []
                        }
                       ]]
      (create agents-data)
      (is (= @agents agents-data)))))

(deftest find-agent-test
  (testing "it can find agent by id"
    (let [agents-data [{
                        :id "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88",
                        :name "Mr. Peanut Butter",
                        :primary_skillset ["rewards-question"],
                        :secondary_skillset ["bills-questions"]
                       }
                       {
                        :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                        :name "BoJack Horseman",
                        :primary_skillset ["bills-questions"],
                        :secondary_skillset []
                        }
                       ]]
      (create agents-data)
      (is (= (find-by-id "ed0e23ef-6c2b-430c-9b90-cd4f1ff74c88") (first @agents))))))
