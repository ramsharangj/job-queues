(ns job-queues.domain.job-test
  (:require [clojure.test :refer :all]
            [job-queues.domain.job :refer :all]
            [job-queues.domain.agent :as agent]
            [job-queues.test-utils :as utils]))

(use-fixtures :each utils/setup-tests)

(deftest create-test
  (testing "it create jobs"
    (let [jobs-data [{"id" "f26e890b-df8e-422e-a39c-7762aa0bac36",
                       "type" "rewards-question",
                       "urgent" false
                     }
                     {"id" "690de6bc-163c-4345-bf6f-25dd0c58e864",
                       "type" "bills-questions",
                       "urgent" true
                     }]]
      (create jobs-data)
      (is (= @jobs jobs-data)))))

(deftest dequeue-test

  (testing "it assigns a job to the agents according to his skills"
    (utils/setup-tests
     (fn []
       (let [jobs-request {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             agent-data [{
                          :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                          :name "BoJack Horseman",
                          :primary_skillset ["bills-questions"],
                          :secondary_skillset []
                          }]
             jobs-data [{
                         :id "f26e890b-df8e-422e-a39c-7762aa0bac36",
                         :type "rewards-question",
                         :urgent true
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-abcdefghijk",
                         :type "bills-questions",
                         :urgent false
                         }]]
         (agent/create agent-data)
         (create jobs-data)
         (is (= (dequeue jobs-request) {:job-id "f26e890b-df8e-422e-a39c-abcdefghijk"
                                        :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}))))))

  (testing "it assigns a job to the agents according to his skills and urgency"
    (utils/setup-tests
     (fn []
       (let [jobs-request {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             agent-data [{
                          :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                          :name "BoJack Horseman",
                          :primary_skillset ["bills-questions"],
                          :secondary_skillset []
                          }]
             jobs-data [{
                         :id "f26e890b-df8e-422e-a39c-7762aa0bac36",
                         :type "bills-questions",
                         :urgent true
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-abcdefghijk",
                         :type "bills-questions",
                         :urgent false
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-zxyghijk",
                         :type "rewards-questions",
                         :urgent false
                         }]]
         (agent/create agent-data)
         (create jobs-data)
         (is (= (dequeue jobs-request) {:job-id "f26e890b-df8e-422e-a39c-7762aa0bac36"
                                        :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}))))))

  (testing "it does not assign a job that is already assigned to an agent"
    (utils/setup-tests
     (fn []
       (let [jobs-request {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             agent-data [{
                          :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                          :name "BoJack Horseman",
                          :primary_skillset ["rewards-questions"],
                          :secondary_skillset []
                          }]
             jobs-data [{
                         :id "f26e890b-df8e-422e-a39c-7762aa0bac36",
                         :type "rewards-questions",
                         :urgent true
                         :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-abcdefghijk",
                         :type "bills-questions",
                         :urgent false
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-zxyghijk",
                         :type "rewards-questions",
                         :urgent false
                         }]]
         (agent/create agent-data)
         (create jobs-data)
         (is (= (dequeue jobs-request) {:job-id "f26e890b-df8e-422e-a39c-zxyghijk"
                                        :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}))))))

  (testing "it assigns a job in secondary skill if it cant find jobs with primary skills"
    (utils/setup-tests
     (fn []
       (let [jobs-request {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             agent-data [{
                          :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                          :name "BoJack Horseman",
                          :primary_skillset ["rewards-questions"],
                          :secondary_skillset ["bills-questions"]
                          }]
             jobs-data [{
                         :id "f26e890b-df8e-422e-a39c-7762aa0bac36",
                         :type "rewards-questions",
                         :urgent true
                         :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-abcdefghijk",
                         :type "bills-questions",
                         :urgent false
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-zxyghijk",
                         :type "rewards-questions",
                         :urgent false,
                         :agent-id "8ab86c18-3fae-4804-bfd9-abcdefghijk"
                        }]]
         (agent/create agent-data)
         (create jobs-data)
         (is (= (dequeue jobs-request) {:job-id "f26e890b-df8e-422e-a39c-abcdefghijk"
                                        :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}))))))

  (testing "it does not assign a job if all the jobs are taken and no jobs are fitting enough"
    (utils/setup-tests
     (fn []
       (let [jobs-request {:agent_id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"}
             agent-data [{
                          :id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260",
                          :name "BoJack Horseman",
                          :primary_skillset ["rewards-questions"],
                          :secondary_skillset []
                          }]
             jobs-data [{
                         :id "f26e890b-df8e-422e-a39c-7762aa0bac36",
                         :type "rewards-questions",
                         :urgent true
                         :agent-id "8ab86c18-3fae-4804-bfd9-abcdef"
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-abcdefghijk",
                         :type "bills-questions",
                         :urgent false
                         }
                        {
                         :id "f26e890b-df8e-422e-a39c-zxyghijk",
                         :type "rewards-questions",
                         :urgent false,
                         :agent-id "8ab86c18-3fae-4804-bfd9-abcdefghijk"
                         }]]
         (agent/create agent-data)
         (create jobs-data)
         (is (= (dequeue jobs-request) {:job-id nil
                                        :agent-id "8ab86c18-3fae-4804-bfd9-c3d6e8f66260"})))))))
