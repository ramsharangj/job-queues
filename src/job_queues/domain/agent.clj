(ns job-queues.domain.agent)

(def agents
  "Using a vector as assumptions is it is sorted by time,
  therefore we need to append to a vector and not prepend"
  (atom []))

(defn- add-to-atom [agent]
  (swap! agents conj agent))

(defn create [agents-data]
  (mapv add-to-atom agents-data))

(defn find-by-id [agent-id]
  (->> @agents
       (filter #(= (:id %) agent-id))
       first))
