(ns job-queues.domain.job
  (:require [job-queues.domain.agent :as agent]))

(def jobs
  "Using a vector as assumptions is it is sorted by time,
  therefore we need to append to a vector and not prepend"
  (atom []))

(defn- add-to-atom [job]
  (swap! jobs conj job))

(defn create [jobs-data]
  (mapv add-to-atom jobs-data))

(defn- not-assigned? [job]
  (not (:agent-id job)))

(defn- get-unassigned-jobs [jobs]
  (->> jobs
       (filter not-assigned?)))

(defn- is-agent-a-junior? [job agent]
  (some #(= (:type job) %) (:secondary_skillset agent)))

(defn- is-agent-a-expert? [job agent]
  (some #(= (:type job) %) (:primary_skillset agent)))

(defn- is-urgent? [job]
  (get job :urgent))

(defn- find-by-primary-skills [agent]
  (->> @jobs
       get-unassigned-jobs
       (filter #(is-agent-a-expert? % agent))))

(defn- find-by-secondary-skills [agent]
  (->> @jobs
       get-unassigned-jobs
       (filter #(is-agent-a-junior? % agent))))

(defn- find-by-skills [agent]
  (let [expert-jobs (find-by-primary-skills agent)
        learning-jobs (find-by-secondary-skills agent)]
    (or (seq expert-jobs) (seq learning-jobs))))

(defn- filter-by-urgency [jobs]
  (let [urgent-jobs (filter is-urgent? (or jobs))]
    (or (seq urgent-jobs) jobs)))

(defn- assign [job agent]
  (let [assigned-job (merge job {:agent-id (:id agent)})]
    (reset! jobs (mapv (fn [job]
                         (if (= (:id assigned-job) (:id job))
                           assigned-job
                           job))
                       @jobs))
    {:job-id (:id assigned-job) :agent-id (:id agent)}))

(defn dequeue [{:keys [agent_id]}]
  (let [agent (agent/find-by-id agent_id)]
    (-> agent
        find-by-skills
        filter-by-urgency
        first
        (assign agent))))
