(ns job-queues.core
  (:gen-class)
  (:require [clojure.data.json :as json]
            [job-queues.domain.job :as job]
            [job-queues.domain.agent :as agent]))

(defn- find-data-by [input key]
  (reduce (fn [initial value]
            (if (get value key)
              (conj initial (get value key))
              initial))
          []
          input))

(defn- seed-data [input]
  (let [jobs-data (find-data-by input :new_job)
        agents-data (find-data-by input :new_agent)]
    (job/create jobs-data)
    (agent/create agents-data)))

(defn- job-requests [input]
  (find-data-by input :job_request))

(defn- format-request [job-request]
  (let [result {:job-assigned (job/dequeue job-request)}]
    (clojure.pprint/pprint (json/write-str result))
    result))

(defn -main
  "Reads from a file and loads data because a DB is not being used"
  [& args]
  (let [input (-> (first args)
                  slurp
                  (json/read-str :key-fn keyword))]
    (seed-data input)
    (mapv format-request (job-requests input))))
