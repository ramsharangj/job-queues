(defproject job-queues "0.1.0-SNAPSHOT"
  :description "A project to assign job requests to agents"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]]
  :main ^:skip-aot job-queues.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
